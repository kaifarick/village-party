﻿using UnityEngine;

public class Mill : MonoBehaviour
{

    Transform fan;

    int speed = -20;
    private void Start()
    {
        fan = GetComponent<Transform>(); 
    }
    void Update()
    {
      fan.Rotate(Vector3.forward * speed * Time.deltaTime);
    }
}
