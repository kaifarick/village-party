﻿using UnityEngine;

public class Bullet : MonoBehaviour
{
    void Update()
    {
        Shoot();     
    }

    private void Shoot()
    {        
        transform.position += new Vector3(0, 7 * Time.deltaTime, 0);

        destroy();
    }

    private void destroy()
    {
        if (transform.position.y > 6f)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        FallDown agenda = collider.GetComponent<FallDown>();
        if (agenda)
        {
            Destroy(gameObject);
        }
    }
}
