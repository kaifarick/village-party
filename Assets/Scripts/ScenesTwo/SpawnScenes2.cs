﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpawnScenes2 : SpawnDangers
{

    public GameObject helicopter;

    void Awake()
    {
        FallDown.fallSpeed = 4f;
    }
    private void Start()
    {
        CreatePool();
    }
    private IEnumerator pool()
    {
        while (!PlayerMain.lose)
        {
            GameObject obj = pool_parent.GetChild(pool_element).gameObject;
            obj.SetActive(true);
            obj.transform.position = transform.position;
            pool_element++;
            if (pool_element > pool_parent.childCount - 1) pool_element = 0;
            yield return new WaitForSeconds(0.5f);
        }
    }

    IEnumerator SpeedPlus()
    {
        while (!PlayerMain.lose)
        {
            end();
            FallDown.fallSpeed += 0.5F;
            if (FallDown.fallSpeed == 6)
            {
                helicopter.SetActive(true);
            }
            yield return new WaitForSeconds(6f);
        }
    }

    public void corutines()
    {
        StartCoroutine(pool());
        StartCoroutine(SpeedPlus());
    }

    private void end()
    {
        if (FallDown.fallSpeed == EndSpeed)
        {
            SceneManager.LoadScene("End");
        }
    }
}
