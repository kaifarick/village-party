﻿using UnityEngine;
using UnityEngine.UI;
using System.Text;

public class Ammo : MonoBehaviour
{
    public Text balance;

    StringBuilder @string = new StringBuilder();

    public void ChangeBalance()
    {
        @string.Clear();
        @string.Append("Боеприпасов Осталось : ");
        @string.Append(Player2.ammunition);
        balance.text = @string.ToString();
    }
}
