﻿using System.Collections;
using UnityEngine;
using UnityEngine.Events;
public class Player2 : Player
{
    GameObject[] diagnoses;

    public static int ammunition;

    public AudioClip ClipShoot;

    public UnityEvent ChangeAmmo;

    public static Vector3 PlayerPosition;

    private void Start()
    {
        PlayerRB = GetComponent<Rigidbody2D>();

        PlayerSR = GetComponent<SpriteRenderer>();

        PlayerCollider = GetComponents<CapsuleCollider2D>();

        anim = GetComponent<Animator>();

        StartCoroutine(ammoPlus());

        ammunition = 1;

        diagnoses = Resources.LoadAll<GameObject>("Diagnoses");
       
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.GetComponent<FallDown>())
        {
            lose = true;
            Restart.SetActive(true);
        }
    }
    private void FixedUpdate()
    {
        PlayerPosition = new Vector3(transform.position.x, 3.8f);
        Move();
    }
    public void Shoot()
    {
        if (lose == false && ammunition != 0)
        {
            int rand = Random.Range(0, diagnoses.Length);
            Instantiate(diagnoses[rand], transform.position, Quaternion.identity);
            AudioManager.Instance.ShootSound.PlayOneShot(ClipShoot,0.7f);
            ammunition--;
            ChangeAmmo.Invoke();
        }
    }        

    IEnumerator ammoPlus()
    {
        while(true)
        {
            if(ammunition < 3 && ammunition > -1)
            ammunition++;
            ChangeAmmo.Invoke();
            yield return new WaitForSeconds(2f);
        }
    }
}
