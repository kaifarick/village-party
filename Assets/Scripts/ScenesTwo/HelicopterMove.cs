﻿using System.Collections;
using UnityEngine;

public class HelicopterMove : MonoBehaviour
{
    Vector3 random;

    private float speed = 3f;

    void Start()
    {
        StartCoroutine(randomPosition());
    }

    void Update()
    {
       Move();
    }

    private void Move()
    {
        transform.position = Vector3.MoveTowards(transform.position, random, Time.deltaTime * speed);
    }

    IEnumerator randomPosition()
    {

        while (!PlayerMain.lose && FallDown.fallSpeed <= 6f)
        {
            random = new Vector3(Random.Range(-10f, 10f), 3.8f);
            yield return new WaitForSeconds(3f);
        }
        while (!PlayerMain.lose && FallDown.fallSpeed > 6f)
        {
            if (Player2.PlayerPosition.x <= 1)
            {
                random = Player2.PlayerPosition;
            }
            else
            {
                random = new Vector3(Random.Range(-10f, 1), 3.8f);
            }
                yield return new WaitForSeconds(2f);
        }
    }
}
