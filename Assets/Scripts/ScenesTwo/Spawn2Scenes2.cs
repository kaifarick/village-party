﻿using System.Collections;
using UnityEngine;

public class Spawn2Scenes2 : SpawnDangers
{
    void Start()
    {
        CreatePool();

        StartCoroutine(pool());
    }
    private IEnumerator pool()
    {
        while (!PlayerMain.lose)
        {
            GameObject obj = pool_parent.GetChild(pool_element).gameObject;
            obj.SetActive(true);
            obj.transform.position = transform.position;
            pool_element++;
            if (pool_element > pool_parent.childCount - 1)
            {
                pool_element = 0;
            }
            yield return new WaitForSeconds(0.5f);
        }
    }
}
