﻿using System.Collections;
using UnityEngine;

public class Helicopter2Move : MonoBehaviour
{
    Vector3 random;

    private float speed = 3f;

    void Start()
    {
        StartCoroutine(randomPosition());
    }
    void Update()
    {
        Move();
    }

    private void Move()
    {
        transform.position = Vector3.MoveTowards(transform.position, random, Time.deltaTime * speed);
    }

    IEnumerator randomPosition()
    {
        while (!PlayerMain.lose)
        {
            if (Player2.PlayerPosition.x >= 1)
            {
                random = Player2.PlayerPosition;
            }
            else
            {
                random = new Vector3(Random.Range(-1, 10f), 3.8f);
            }
                yield return new WaitForSeconds(2f);
        }
    }
}
