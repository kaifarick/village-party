﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Handbook : MonoBehaviour, IPointerClickHandler
{
    public Transform Parent;

    public void OnPointerClick(PointerEventData eventData)
    {
        for (int i = 0; i < Parent.childCount; i++)
        {
            Parent.GetChild(i).gameObject.SetActive(true);
        }
    }

    public void OffBook()
    {
        for (int i = 0; i < Parent.childCount; i++)
        {
            Parent.GetChild(i).gameObject.SetActive(false);
        }
    }
}
