﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class EndCamera : MonoBehaviour
{

    private Camera FinalCam;

    private float timeStart = 0f;

    private void Start()
    {
        AudioManager.Instance.foxSays.enabled = false;

        Screen.orientation = ScreenOrientation.Portrait;

        FinalCam = GetComponent<Camera>();
    }

    void Update()
    {
        if (FinalCam.orthographicSize != 5)
        {
            FinalCam.orthographicSize -= 0.5f;
        }

        timeStart += Time.deltaTime;

        if (Input.touchCount > 0 && timeStart >= 3f)
        {
            SceneManager.LoadScene("Menu");
        }
    }
}
