﻿using UnityEngine;

public class FallDown : MonoBehaviour
{
    public static float fallSpeed;

    private BoxCollider2D box;

    public Rigidbody2D gravity;

    private void Start()
    {
       box = GetComponent<BoxCollider2D>();

       gravity = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        Fall();
    }

    private void Fall()
    {
        if (!PlayerMain.lose)
        {
            transform.position -= new Vector3(0, fallSpeed * Time.deltaTime, 0);

            destroy();
        }

        else
        {
            box.isTrigger = false;
            gravity.gravityScale = 2f;
        }
    }

       private void destroy()
    {
        if (transform.position.y < -6f)
        {
           gameObject.SetActive(false);
        }
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        Bullet bullet = collider.GetComponent<Bullet>();
        if (bullet)
        {
            gameObject.SetActive(false);
        }
    }
}
    
