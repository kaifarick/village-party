﻿using UnityEngine;

public class AudioManager : Singleton<AudioManager>
{
    public AudioSource foxSays;
    public AudioSource ShootSound;
}