﻿using UnityEngine;

public class DownCollider : MonoBehaviour
{
    BoxCollider2D box;

    void Awake()
    {
        box = GetComponent<BoxCollider2D>();
        box.enabled = false;
    }

    void Update()
    {
        if (PlayerMain.lose)
        {
            BoxCollider2D box = GetComponent<BoxCollider2D>();
            box.enabled = true;
        }
    }
}
    


