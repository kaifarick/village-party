﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SpawnDangers :MonoBehaviour
{
    
    private int pool_count = 10;

    public GameObject prefab;

    public Transform pool_parent;

    [HideInInspector] public int pool_element = 0;

    private GameObject[] pool_array;

    public int EndSpeed;


    private void Start()
    {
        FallDown.fallSpeed = 4f;

        CreatePool();

        Invoke("coroutine", 4f);

    }

    public void CreatePool()
    {
        pool_array = new GameObject[pool_count];
        for (int i = 0; i < pool_count; i++)
        {
            pool_array[i] = Instantiate(prefab, new Vector2(Random.Range(-2.4f, 2.3f), 9), Quaternion.identity, pool_parent);
            pool_array[i].SetActive(false);
        }
    }

    private IEnumerator pool()
    {
        while (!PlayerMain.lose && FallDown.fallSpeed <= 6f)
        {
            GameObject obj = pool_parent.GetChild(pool_element).gameObject;
            obj.SetActive(true);
            obj.transform.position = new Vector2(Random.Range(-2.4f, 2.3f), 8);
            pool_element++;
            if (pool_element > pool_parent.childCount - 1)
            {
                pool_element = 0;
            }
            yield return new WaitForSeconds(1);
        }

        while (!PlayerMain.lose && FallDown.fallSpeed > 6f)
        {
            if (FallDown.fallSpeed == EndSpeed)
            {
                SceneManager.LoadScene("ScenesTwo");
            }
            GameObject obj = pool_parent.GetChild(pool_element).gameObject;
            obj.SetActive(true);
            obj.transform.position = new Vector2(Random.Range(-2.4f, 2.3f), 8);
            pool_element++;
            if (pool_element > pool_parent.childCount - 1)
            {
                pool_element = 0;
            }
            yield return new WaitForSeconds(0.7f);
        }
    }

    IEnumerator SpeedPlus()
    {
        while (!PlayerMain.lose)
        {
            FallDown.fallSpeed += 0.5F;
            yield return new WaitForSeconds(3f);
        }
    }

    void coroutine()
    {
        AudioManager.Instance.foxSays.enabled = true;
        StartCoroutine(pool());
        StartCoroutine(SpeedPlus());
    }
}




