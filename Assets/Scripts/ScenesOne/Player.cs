﻿using UnityEngine;
using UnityEngine.Events;

public class Player : PlayerMain
{
    public GameObject Restart;

    public SpriteRenderer PlayerSR;

    public CapsuleCollider2D[] PlayerCollider;

    public Rigidbody2D PlayerRB;

    public Animator anim;

    public int MoveSpeed;


    private void Start()
    {

        PlayerRB = GetComponent <Rigidbody2D>();

        PlayerSR = GetComponent<SpriteRenderer>();

        PlayerCollider = GetComponents<CapsuleCollider2D>();

        anim = GetComponent<Animator>();

    }

    void OnTriggerEnter2D (Collider2D other)
    {
        if (other.gameObject.GetComponent<FallDown>())
        { 
            lose = true;
            Restart.SetActive(true);            
        }
    }

    private void FixedUpdate()
    {
        Move();
    }

    public void Move()
    {
        if (lose == false)
        {
            float Horiz = Input.GetAxis("Horizontal");
            PlayerRB.velocity = new Vector3(Horiz, 0f, 0f) * MoveSpeed;

            if (Horiz < 0)
            {
                PlayerSR.flipX = true;
                PlayerCollider[0].enabled = true;
                PlayerCollider[1].enabled = false;
            }

            else if (Horiz > 0)
            {
                PlayerSR.flipX = false;
                PlayerCollider[0].enabled = false;
                PlayerCollider[1].enabled = true;
            }
        }

        else
        {
            PlayerRB.velocity = new Vector3(0, 0, 0);
            anim.SetBool("loseAM", true);
            PlayerRB.constraints = RigidbodyConstraints2D.None;
            AudioManager.Instance.foxSays.enabled = false;
        }
    }

    public void MovePhone()
    {
        if (lose == false)
        {
            Vector3 acc = Input.acceleration;
            PlayerRB.velocity = new Vector3(acc.x, 0f, 0f) * MoveSpeed;

            if (acc.x < -0.03)
            {
                PlayerSR.flipX = true;
                PlayerCollider[0].enabled = true;
                PlayerCollider[1].enabled = false;
            }

            else if (acc.x > 0.03)
            {
                PlayerSR.flipX = false;
                PlayerCollider[0].enabled = false;
                PlayerCollider[1].enabled = true;
            }
        }

        else
        {
            PlayerRB.velocity = new Vector3(0, 0, 0);
            anim.SetBool("loseAM", true);
            PlayerRB.constraints = RigidbodyConstraints2D.None;
            AudioManager.Instance.foxSays.enabled = false;
        }
    }
}
