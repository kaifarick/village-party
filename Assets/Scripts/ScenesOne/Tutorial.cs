﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Tutorial : MonoBehaviour
{
    int i = 4;

    private Text textCount;

    void Start()
    {
        textCount = GetComponentInChildren<Text>();
        Invoke("off", 4f);
        StartCoroutine(counting());
    }

    private void off()
    {
        Destroy(gameObject);
    }       
    
    private IEnumerator counting()
    {
        while (i != 0)
        {
            textCount.text = i.ToString();
            i--;
            yield return new WaitForSeconds(1.2f);
        }
    }
}