﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class InScenesOne : MonoBehaviour, IPointerClickHandler
{
public void OnPointerClick(PointerEventData eventData)

    {
        SceneManager.LoadScene("ScenesOne");
    }

    private void Start()
    {
        transform.SetAsLastSibling();
    }
}
