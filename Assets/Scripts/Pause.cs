﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour, IPointerClickHandler
{

    public Transform Parent;

    public void OnPointerClick(PointerEventData eventData)
    {
        for (int i = 0; i < Parent.childCount; i++)
        {
            Parent.GetChild(i).gameObject.SetActive(true);
            Time.timeScale = 0;
            AudioListener.pause = true;
        }
    }

    public void OffPause()
    {
        for (int i = 0; i < Parent.childCount; i++)
        {
            Parent.GetChild(i).gameObject.SetActive(false);
            Time.timeScale = 1;
            AudioListener.pause = false;
        }
    }

    public void inMenu()
    {
        SceneManager.LoadScene("Menu");
        AudioManager.Instance.foxSays.enabled = false;
        Time.timeScale = 1;
        AudioListener.pause = false;
    }

    public void Exit()
    {
        Application.Quit();
    }
}
